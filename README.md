# OpenML dataset: traffic_violations_100k

https://www.openml.org/d/43641

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This dataset contains traffic violation information from all electronic traffic violations issued in the County. Any information that can be used to uniquely identify the vehicle, the vehicle owner or the officer issuing the violation will not be published.
Acknowledgements
source of original dataset: https://catalog.data.gov/dataset/traffic-violations-56dda
Inspiration
The original dataset was just too time-consuming to perform several basic tasks. Thus, I shuffled it and took the first 100k rows.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43641) of an [OpenML dataset](https://www.openml.org/d/43641). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43641/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43641/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43641/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

